# Trial of Backend
Welcome and thank you for accepting the Backend trial of Bublar!

For clarification, questions or anything else please [send us an email!](mailto:incoming+bublar-trials-backend-20074044-issue-@incoming.gitlab.com)
## Instructions
Create a **private** fork of this repository, and once the assignment is complete share it with GitLab users @adriano.bublar and @aschwel.

Please complete the assignment in [Go](https://golang.org) unless something else was agreed upon during the interview process.

## Assignment
Create a backend that serves parts of an API for a location based game. 

For data storage you are free to choose whatever solution you want (SQL/NoSQL/Key-Value/Anything else), but consider how you would make it simple to switch to a different storage solution.

Similarly, the API can be served over HTTP or any other well-known protocol, but think about how you would make it simple to change and maybe even support clients with multiple protocols.

The solution should include a simple way of running the full stack.

The backend consists of: 
* Players
* Locations

These are accessed via two endpoints:
* Admin 
* Client

### Players
A Player is a:
* Username
* Password
* Current Location

#### Client endpoints
* Public
  * Register (username & password)
  * Login (username & password)

* Authenticated
  * Set Current Location

Authenticate using a **JSON Web Token**.

#### Admin endpoints
* Public
  * Delete

### Locations
A Location consists of:
* Unique identifier
* Geographic coordinates (Latitude/Longitude) 
* Name

#### Client endpoints
* Authenticated
  * Read

#### Admin endpoints
* Public
  * Create
  * Edit
  * Read
  * Delete    

## For Your Consideration
Admin endpoints don’t have authentication, what options exist to protect them from being publicly accessible?

Please write at least one test to demonstrate your approach to testing. 
