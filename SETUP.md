Trial of Backend
================

# Prerequesites
First run this command (requires docker)
```
docker-compose up
```

create an .env file in the base folder like this:
```
ACCESS_SECRET=randomstuff
MONGO_DATABASE_NAME=trial
MONGO_DATABASE_URI=mongodb://root:password@localhost:27017
```

# Start the app

Start application with `go run main` or start debugging inside VSCode

# Test the app

In your console

`curl -d '{"username":"torsten", "password":"1234"}' -H 'Content-Type: application/json' http://localhost:8080/register`

to register a new user. Use `/login` to login and `/delete` to remove a player using the above json payload

# Auth & Afterwords

The app uses BasicAuth for admin authentication.

The app also includes a simple password verificator hash+salt generator (bcrypt) which wasn't included in the scope but I consider this of high importance of a functioning (live) system.