package api

import (
	"log"
	"net/http"
	"os"
	"time"
	"trial/v2/internal/trial/types"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// API interface
type API interface {
	Setup()
	PlayerLogin(fn func(pw string, usr string) bool)
	PlayerRegister(fn func(pw string, usr string) bool)
	PlayerDelete(fn func(user string) bool)
	PlayerSetLocation(fn func(user string, lng float64, lat float64) bool)
	PlayerRead(fn func(user string) types.Player)
	LocationCreate(fn func(location types.Location) bool)
	LocationEdit(fn func(location Location) bool)
	LocationRead(fn func(id string) types.Location)
	LocationDelete(fn func(location types.Location) bool)
	Listen()
}

// HTTPAPI implementation (should be put in separate file but for this scenario there is only one implementation)
type HTTPAPI struct {
	playerLoginInternal       func(password string, username string) bool
	playerRegisterInternal    func(password string, username string) bool
	playerDeleteInternal      func(user string) bool
	playerSetLocationInternal func(user string, lng float64, lat float64) bool
	playerReadInternal        func(user string) types.Player
	locationCreateInternal    func(location types.Location) bool
	locationEditInternal      func(location Location) bool
	locationReadInternal      func(id string) types.Location
	locationDeleteInternal    func(location types.Location) bool
	router                    *gin.Engine
}

// Login http body
type Login struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password"`
}

// Location http body
type Location struct {
	ID  string  `json:"_id"`
	Lng float64 `json:"lng" binding:"required"`
	Lat float64 `json:"lat" binding:"required"`
}

// Claims JWT
type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

var (
	identityKey = "id"
	secretKey   = os.Getenv("ACCESS_SECRET")
	jwtKey      = []byte("terrible_secret")
)

// ValidateJwt non-standard
func ValidateJwt(c *gin.Context) (string, error) {
	cookie, err := c.Request.Cookie("token")

	// cookie missing
	if err != nil {
		return "", err
	}

	tkn := cookie.Value
	claims := &Claims{}

	key, err := jwt.ParseWithClaims(tkn, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	// cookie parse failed
	if err != nil {
		return "", err
	}

	// cookie invalid
	if !key.Valid {
		return "", nil
	}

	return claims.Username, nil
}

// Setup the http server
func (httpapi *HTTPAPI) Setup() {
	router := gin.Default()
	httpapi.router = router

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	router.POST("client/player/login", func(c *gin.Context) {
		var json Login
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ok := httpapi.playerLoginInternal(json.Password, json.Username)
		// login is successful
		if ok {
			expirationTime := time.Now().Add(5 * time.Minute)

			claims := &Claims{
				Username: json.Username,
				StandardClaims: jwt.StandardClaims{
					ExpiresAt: expirationTime.Unix(),
				},
			}

			token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
			tokenString, err := token.SignedString(jwtKey)

			http.SetCookie(c.Writer, &http.Cookie{
				Name:    "token",
				Value:   tokenString,
				Expires: expirationTime,
			})

			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
				return
			}

		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	router.POST("client/player/register", func(c *gin.Context) {
		var json Login
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ok := httpapi.playerRegisterInternal(json.Password, json.Username)
		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "you are logged in"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	router.POST("client/player/setlocation", func(c *gin.Context) {
		var json Location
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		user, err := ValidateJwt(c)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		ok := httpapi.playerSetLocationInternal(user, json.Lng, json.Lat)
		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "player location updated"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	router.POST("client/player/location", func(c *gin.Context) {
		var json Login
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		user, err := ValidateJwt(c)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}

		player := httpapi.playerReadInternal(user)
		if player.Username != "" {
			c.JSON(http.StatusOK, gin.H{"location": player.Loc})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	admin := router.Group("/admin", gin.BasicAuth(gin.Accounts{
		"foo": "bar",
	}))

	// check/test BasicAuth
	admin.GET("/secrets", func(c *gin.Context) {
		// get user, it was set by the BasicAuth middleware
		user := c.MustGet(gin.AuthUserKey).(string)
		if user != "" {
			c.JSON(http.StatusOK, gin.H{"user": user})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	admin.POST("player/delete", func(c *gin.Context) {
		var json Login
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ok := httpapi.playerDeleteInternal(json.Username)
		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "player deleted"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	// "/admin/"
	admin.POST("location/create", func(c *gin.Context) {
		var json types.Location
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ok := httpapi.locationCreateInternal(json)
		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "location created"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	admin.POST("location/edit", func(c *gin.Context) {
		var json Location
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ok := httpapi.locationEditInternal(json)

		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "location edited"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	admin.POST("location/read", func(c *gin.Context) {
		var json types.Location
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		location := httpapi.locationReadInternal(json.ID)
		if location.ID != "" {
			c.JSON(http.StatusOK, gin.H{"location": location})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

	admin.POST("location/delete", func(c *gin.Context) {
		var json types.Location
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		ok := httpapi.locationDeleteInternal(json)
		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "location deleted"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal error"})
		}
	})

}

// PlayerLogin .
func (httpapi *HTTPAPI) PlayerLogin(loginfunc func(pw string, usr string) bool) {
	httpapi.playerLoginInternal = loginfunc
}

// PlayerRegister .
func (httpapi *HTTPAPI) PlayerRegister(registerfunc func(pw string, usr string) bool) {
	httpapi.playerRegisterInternal = registerfunc
}

// PlayerDelete .
func (httpapi *HTTPAPI) PlayerDelete(deletefunc func(user string) bool) {
	httpapi.playerDeleteInternal = deletefunc
}

// PlayerSetLocation .
func (httpapi *HTTPAPI) PlayerSetLocation(setlocationfunc func(user string, lng float64, lat float64) bool) {
	httpapi.playerSetLocationInternal = setlocationfunc
}

// PlayerRead .
func (httpapi *HTTPAPI) PlayerRead(readfunc func(user string) types.Player) {
	httpapi.playerReadInternal = readfunc
}

// LocationCreate .
func (httpapi *HTTPAPI) LocationCreate(createfunc func(location types.Location) bool) {
	httpapi.locationCreateInternal = createfunc
}

// LocationEdit .
func (httpapi *HTTPAPI) LocationEdit(editfunc func(location Location) bool) {
	httpapi.locationEditInternal = editfunc
}

// LocationRead .
func (httpapi *HTTPAPI) LocationRead(readfunc func(id string) types.Location) {
	httpapi.locationReadInternal = readfunc
}

// LocationDelete .
func (httpapi *HTTPAPI) LocationDelete(deletefunc func(location types.Location) bool) {
	httpapi.locationDeleteInternal = deletefunc
}

// Listen used for http implementation
func (httpapi *HTTPAPI) Listen() {
	log.Fatal(httpapi.router.Run(":8080"))
}
