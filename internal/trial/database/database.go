package database

import (
	"context"
	"log"
	"time"
	"trial/v2/internal/trial/api"
	"trial/v2/internal/trial/types"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	playerCol   = "players"
	locationCol = "locations"
)

// Database API
type Database interface {
	Connect(uri string, dbname string) (bool, error)
	CreatePlayer(types.Player) (bool, error)
	DeletePlayer(types.Player) (bool, error)
	FindPlayer(username string) types.Player
	SetPlayerLocation(types.Player, types.Location) (bool, error)
	CreateLocation(types.Location) (bool, error)
	EditLocation(api.Location) (bool, error)
	FindLocation(id string) types.Location
	DeleteLocation(types.Location) (bool, error)
}

// MongoDatabase Mongo API implementation
type MongoDatabase struct {
	client   *mongo.Client
	database *mongo.Database
}

// Connect to mongodb
func (mdb *MongoDatabase) Connect(uri string, dbname string) (bool, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return false, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return false, err
	}

	mdb.database = client.Database(dbname)
	mdb.client = client

	return true, nil
}

// CreatePlayer creates player with an unique name
func (mdb *MongoDatabase) CreatePlayer(player types.Player) (bool, error) {
	collection := mdb.database.Collection(playerCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	filter := bson.M{"username": bson.M{"$eq": player.Username}}
	p := collection.FindOne(ctx, filter)
	v := new(types.Player)
	p.Decode(&v)
	if v.Username == "" {
		_, err := collection.InsertOne(ctx, player)
		if err != nil {
			return false, err
		}
		return true, nil
	}

	return false, nil
}

// DeletePlayer deletes player
func (mdb *MongoDatabase) DeletePlayer(player types.Player) (bool, error) {
	collection := mdb.database.Collection(playerCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.DeleteOne(ctx, player)
	if err != nil {
		return false, err
	}

	return true, nil
}

// FindPlayer finds a player
func (mdb *MongoDatabase) FindPlayer(username string) types.Player {
	collection := mdb.database.Collection(playerCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	filter := bson.M{"username": bson.M{"$eq": username}}
	p := collection.FindOne(ctx, filter)
	v := new(types.Player)
	p.Decode(&v)
	if v.Username == "" {
		return types.Player{}
	}
	return *v
}

// SetPlayerLocation updates player location
func (mdb *MongoDatabase) SetPlayerLocation(player types.Player, newLoc types.Location) (bool, error) {
	collection := mdb.database.Collection(playerCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	filter := bson.M{"username": bson.M{"$eq": player.Username}}
	update := bson.M{"$set": bson.M{"loc": newLoc}}
	_, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}

	return true, nil
}

// CreateLocation .
func (mdb *MongoDatabase) CreateLocation(location types.Location) (bool, error) {
	collection := mdb.database.Collection(locationCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	parsedID, _ := primitive.ObjectIDFromHex(location.ID)
	filter := bson.M{"_id": bson.M{"$eq": parsedID}}
	p := collection.FindOne(ctx, filter)
	v := new(types.Location)
	p.Decode(&v)
	if v.ID == "" {
		_, err := collection.InsertOne(ctx, location)
		if err != nil {
			return false, err
		}
		return true, nil
	}

	return false, nil
}

// EditLocation .
func (mdb *MongoDatabase) EditLocation(location api.Location) (bool, error) {
	collection := mdb.database.Collection(locationCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	parsedID, err := primitive.ObjectIDFromHex(location.ID)
	filter := bson.M{"_id": bson.M{"$eq": parsedID}}
	update := bson.M{"$set": bson.M{"lng": location.Lng, "lat": location.Lat}}
	result, err := collection.UpdateOne(ctx, filter, update)
	log.Printf("%+v", result)
	if err != nil {
		return false, err
	}

	return true, nil
}

// FindLocation .
func (mdb *MongoDatabase) FindLocation(id string) types.Location {
	collection := mdb.database.Collection(locationCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	parsedID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": bson.M{"$eq": parsedID}}
	p := collection.FindOne(ctx, filter)
	v := new(types.Location)
	p.Decode(&v)
	if v.ID != "" {
		return *v
	}
	return types.Location{}
}

// DeleteLocation .
func (mdb *MongoDatabase) DeleteLocation(location types.Location) (bool, error) {
	collection := mdb.database.Collection(locationCol)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	parsedID, _ := primitive.ObjectIDFromHex(location.ID)
	filter := bson.M{"_id": bson.M{"$eq": parsedID}}
	_, err := collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}

	return true, nil
}
