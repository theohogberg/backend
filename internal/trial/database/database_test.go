package database

import (
	"os"
	"testing"
	"trial/v2/internal/trial/types"

	"github.com/google/uuid"
)

// Test insert and update player
func TestPlayer(t *testing.T) {
	os.Setenv("MONGO_DATABASE_NAME", "test")
	os.Setenv("MONGO_DATABASE_URI", "mongodb://root:password@localhost:27017")

	mdb := new(MongoDatabase)
	mdb.Connect(os.Getenv("MONGO_DATABASE_URI"), os.Getenv("MONGO_DATABASE_NAME"))

	ID, _ := uuid.NewUUID()
	loc := types.Location{
		ID:  ID,
		Lng: 44.3553,
		Lat: 23.2323,
	}

	hash := []byte("asdjksakdh")
	playerDoc := types.Player{
		Username: "user1",
		Hash:     hash,
		Loc:      loc,
	}

	t.Logf("Insert: %v", playerDoc)
	mdb.SavePlayer(playerDoc)

	newID, _ := uuid.NewUUID()
	newLoc := types.Location{
		ID:  newID,
		Lng: 43.235235,
		Lat: 31.151515,
	}

	t.Logf("Update location to: %v", newLoc)
	mdb.SetPlayerLocation(playerDoc, newLoc)
}
