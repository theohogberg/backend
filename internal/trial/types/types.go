package types

// Player the player
type Player struct {
	Username string
	Hash     []byte
	Loc      Location
}

// Location http body
type Location struct {
	ID  string  `json:"uuid,omitempty"`
	Lng float64 `json:"lng"`
	Lat float64 `json:"lat"`
}
