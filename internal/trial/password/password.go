package password

import (
	"golang.org/x/crypto/bcrypt"
)

// GeneratePassword password string
func GeneratePassword(pw []byte) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword(pw, bcrypt.MinCost)
	if err != nil {
		return []byte{}, err
	}

	return hash, nil
}

// ComparePassword ok bool
func ComparePassword(hash string, input []byte) (bool, error) {
	byteHash := []byte(hash)
	err := bcrypt.CompareHashAndPassword(input, byteHash)
	if err != nil {
		return false, err
	}

	return true, nil
}
