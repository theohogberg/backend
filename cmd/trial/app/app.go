package app

import (
	"log"
	"os"
	"trial/v2/internal/trial/api"
	"trial/v2/internal/trial/database"
	"trial/v2/internal/trial/password"
	"trial/v2/internal/trial/types"
)

type app struct {
	db  database.Database
	api api.API
}

var (
	generatePassword = password.GeneratePassword
	comparePassword  = password.ComparePassword
)

var appl app

// Setup decide api & database & glue it together
func Setup() {
	appl = *new(app)
	appl.db = database.Database(new(database.MongoDatabase))
	appl.db.Connect(os.Getenv("MONGO_DATABASE_URI"), os.Getenv("MONGO_DATABASE_NAME"))
	appl.api = api.API(new(api.HTTPAPI))
	appl.api.Setup()

	appl.api.PlayerRegister(func(password string, user string) bool {
		hash, err := generatePassword([]byte(password))
		if err != nil {
			return false
		}
		player := types.Player{Username: user, Hash: hash}
		saved, _ := appl.db.CreatePlayer(player)
		return saved
	})

	appl.api.PlayerLogin(func(password string, user string) bool {
		dbhash := appl.db.FindPlayer(user).Hash
		ok, err := comparePassword(password, dbhash)
		if err != nil {
			log.Println(err)
		}
		return ok
	})

	appl.api.PlayerDelete(func(user string) bool {
		player := appl.db.FindPlayer(user)
		ok, err := appl.db.DeletePlayer(player)
		if err != nil {
			log.Println(err)
		}
		return ok
	})

	appl.api.PlayerSetLocation(func(user string, lng float64, lat float64) bool {
		player := appl.db.FindPlayer(user)
		loc := types.Location{
			Lng: lng,
			Lat: lat,
		}

		ok, err := appl.db.SetPlayerLocation(player, loc)
		if err != nil {
			log.Println(err)
		}
		return ok
	})

	appl.api.PlayerRead(func(user string) types.Player {
		player := appl.db.FindPlayer(user)
		return player
	})

	appl.api.LocationCreate(func(location types.Location) bool {
		ok, err := appl.db.CreateLocation(location)
		if err != nil {
			log.Println(err)
		}
		return ok
	})

	appl.api.LocationEdit(func(location api.Location) bool {
		ok, err := appl.db.EditLocation(location)
		if err != nil {
			log.Println(err)
		}
		return ok
	})

	appl.api.LocationRead(func(id string) types.Location {
		location := appl.db.FindLocation(id)
		return location
	})

	appl.api.LocationDelete(func(location types.Location) bool {
		ok, err := appl.db.DeleteLocation(location)
		if err != nil {
			log.Println(err)
		}
		return ok
	})

	appl.api.Listen()
}
