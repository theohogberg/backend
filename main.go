package main

import (
	"log"
	"trial/v2/cmd/trial/app"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	log.Println("Env loaded.")
	app.Setup()
	// log.Println("Application setup.")
}
